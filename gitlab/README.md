# GitLab Registry

```
cd certs
```

```
docker cp gitlab:/var/opt/gitlab/gitlab-rails/etc/gitlab-registry.key .
openssl req  -key gitlab-registry.key -new -subj "/CN=gitlab-issuer" -x509 -days 365 -out gitlab-registry.crt
```

# GitLab Runner

```
version: '3.3'
services:
  gitlab_runner:
    container_name: gitlab_runner
    image: 'gitlab/gitlab-runner:latest'
    restart: always
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./config:/etc/gitlab-runner
    networks:
      - traefik-public

networks:
  traefik-public:
    external: true
```

# GitLab LDAP

```
        # Gitlab LDAP
        gitlab_rails['gitlab_signin_enabled'] = true
        gitlab_rails['ldap_enabled'] = true
        gitlab_rails['prevent_ldap_sign_in'] = false
        gitlab_rails['ldap_servers'] = {
        'main' => {
          'label' => 'LDAP',
          'host' =>  '',
          'port' => 389,
          'uid' => 'sAMAccountName',
          'verify_certificates' => false,
          'bind_dn' => '',
          'password' => '',
          'timeout' => 10,
          'encryption' => 'plain',
          'active_directory' => true,
          'allow_username_or_email_login' => true,
          'block_auto_created_users' => true,
          'base' => '',
          'lowercase_usernames' => true,
          'user_filter' => ''
          }
        }
```

# GitLab KAS

```
        # Gitlab Kas
        gitlab_kas['enable'] = true
        gitlab_kas['gitlab_address'] = 'https://gitlab.example.com'
```
